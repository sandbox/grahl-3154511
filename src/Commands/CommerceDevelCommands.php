<?php

namespace Drupal\commerce_devel_generate\Commands;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\Component\Utility\Random;
use Drupal\profile\Entity\Profile;
use Drush\Commands\DrushCommands;
use Faker\Factory;

/**
 * Commerce Devel commands.
 */
class CommerceDevelCommands extends DrushCommands {

  /**
   * Generate orders.
   *
   * @param int $count
   *   Argument description.
   *
   * @usage commerce_devel_generate-generateOrders 10
   *   Amount of orders to generate, default 10.
   *
   * @command commerce_devel_generate:generate-orders
   * @aliases cdgo
   */
  public function generateOrders($count = 10): void {
    for ($i = 0; $i < $count; $i++) {
      $this->generateOrder();
    }
  }

  /**
   * Generate one order with random data from a list of fixed locales.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function generateOrder(): void {
    $locales = ['de_CH', 'de_DE', 'en_US'];
    $currencies = ['CHF', 'USD', 'EUR', 'JPY'];
    $random = new Random();
    $faker = Factory::create($locales[array_rand($locales)]);
    $profile = Profile::create(
      [
        'type' => 'customer',
        'uid' => 0,
        'address' => [
          'country_code' => 'CH',
          'postal_code' => 8048,
          'locality' => $faker->city,
          'address_line1' => $faker->streetAddress,
          'given_name' => $faker->firstName,
          'family_name' => $faker->lastName,
        ],
      ]
    );
    $order_item = OrderItem::create(
      [
        'type' => 'default',
        'title' => $random->sentences('1'),
        'quantity' => random_int(1, 5),
        'unit_price' => [
          'number' => (string) random_int(1, 1000),
          'currency_code' => $currencies[array_rand($currencies)],
        ],
      ]
    );
    $order_item->save();

    $order = Order::create(
      [
        'type' => 'default',
        'store_id' => 1,
        'uid' => 1,
        'billing_profile' => $profile,
        'shipping_profile' => $profile,
        'order_items' => [$order_item],
        'state' => 'completed',
      ]
    );
    $order->save();
    $this->logger()->success(dt('Generated order @id', ['@id' => $order->id()]));
  }

}
